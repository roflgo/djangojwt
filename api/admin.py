from django.contrib import admin

from api.models import User


@admin.register(User)
class UserRegister(admin.ModelAdmin):
    list_display = ("email", "first_name", "last_name")
